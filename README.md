# JEAR

Jupyter (Public) Empty Arm32 Repository (Démo)

En statique, ou dynamique (avec binder ci-dessous) :
* un modèle de programme simple "bonjour.s"
* un modèle de programme simple "plusUn.s"
* un modèle de programme simple un peu plus interactif "bonjourPlusUnPlusInteractif.s"

Pour la version dynamique (la plus intéressante), si vous êtes déjà adepte de Jupyter, vous pouvez essayer de lancer ces notebook sur votre machine (attention, il y a des prérequis, regarder apt.txt et requirements.txt) ou choisissez votre serveur Binder :
* Binder sur gricad (**version conseillée**, pour ceux qui ont un vpn uga) : [![Binder](https://binderhub.univ-grenoble-alpes.fr/badge_logo.svg)](https://binderhub.univ-grenoble-alpes.fr/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdenisb%2Fjear/HEAD)
* myBynder (pour les autres, en espérant que myBinder sera disponible): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdenisb%2Fjear/HEAD)
